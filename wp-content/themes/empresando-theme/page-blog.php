<?php
/**
 * Template Name: Blog
 */
 
 get_header(); ?>
 
 <div class="post-list">
<?php
//The Query
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$new_query = new WP_Query();
$new_query->query( 'showposts=4&paged='.$paged );

//The Loop
while ($new_query->have_posts()) : $new_query->the_post();

?>

<div class="post">
	<div class="page-head">
	<h1 class="page-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
	</div>
	
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
		
		<?php posted_on(); ?>
	
		<div class="entry">
			<?php 
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				?>
				<div class="featured-image">
				<?
				the_post_thumbnail( 'thumbnail' );
				?>
				</div>
				<?
			} 
			?>
			<?php the_excerpt(); ?>
			
			<?php include (TEMPLATEPATH . '/social-buttons/share.php'); ?>
		</div>
	
		<footer class="postmetadata">
			<?php the_tags(__('Tags: ','html5reset'), ', ', '<br />'); ?>
			<!--<?php _e('Posted in','html5reset'); ?> <?php the_category(', ') ?> |--> 
		</footer>
	
	</article>
</div>
<?
endwhile;


?>
<div class="posts_link_nav">
<?
next_posts_link('&laquo; Meno recenti', $new_query->max_num_pages);
previous_posts_link('Pi&ugrave; recenti &raquo;');
?>
</div>
<?
?>
 </div>
 


<?php get_footer(); ?>
