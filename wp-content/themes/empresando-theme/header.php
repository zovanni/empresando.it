<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?><!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head id="<?php echo of_get_option('meta_headid'); ?>" data-template-set="html5-reset-wordpress-theme">

	<meta charset="<?php bloginfo('charset'); ?>">
	
	
	<!-- Always force latest IE rendering engine (even in intranet) -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

	<!--Google will often use this as its description of your page/site. Make it good.-->
	<meta name="description" content="<?php bloginfo('description'); ?>" />

	<?php
		if (true == of_get_option('meta_author'))
			echo '<meta name="author" content="' . of_get_option("meta_author") . '" />';

		if (true == of_get_option('meta_google'))
			echo '<meta name="google-site-verification" content="' . of_get_option("meta_google") . '" />';
	?>

	<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">

	<?php
		/*
			j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
			 - device-width : Occupy full width of the screen in its current orientation
			 - initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
			 - maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
		*/
		if (true == of_get_option('meta_viewport'))
			echo '<meta name="viewport" content="' . of_get_option("meta_viewport") . '" />';
	?>
	
	


	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/reset.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fonts/fonts.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/typography.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" />

	<!-- Lea Verou's Prefix Free, lets you use only un-prefixed properties in yuor CSS files -->
    <script src="<?php echo get_template_directory_uri(); ?>_/js/prefixfree.min.js"></script>

	<!-- This is an un-minified, complete version of Modernizr.
		 Before you move to production, you should generate a custom build that only has the detects you need. -->
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/modernizr-2.8.0.dev.js"></script>
	
	
	
	

	<!-- Application-specific meta tags -->
	<?php
		// Windows 8
		if (true == of_get_option('meta_app_win_name')) {
			echo '<meta name="application-name" content="' . of_get_option("meta_app_win_name") . '" /> ';
			echo '<meta name="msapplication-TileColor" content="' . of_get_option("meta_app_win_color") . '" /> ';
			echo '<meta name="msapplication-TileImage" content="' . of_get_option("meta_app_win_image") . '" />';
		}

		// Twitter
		if (true == of_get_option('meta_app_twt_card')) {
			echo '<meta name="twitter:card" content="' . of_get_option("meta_app_twt_card") . '" />';
			echo '<meta name="twitter:site" content="' . of_get_option("meta_app_twt_site") . '" />';
			echo '<meta name="twitter:title" content="' . of_get_option("meta_app_twt_title") . '">';
			echo '<meta name="twitter:description" content="' . of_get_option("meta_app_twt_description") . '" />';
			echo '<meta name="twitter:url" content="' . of_get_option("meta_app_twt_url") . '" />';
		}

		// Facebook
		if (true == of_get_option('meta_app_fb_title')) {
			echo '<meta property="og:title" content="' . of_get_option("meta_app_fb_title") . '" />';
			echo '<meta property="og:description" content="' . of_get_option("meta_app_fb_description") . '" />';
			echo '<meta property="og:url" content="' . of_get_option("meta_app_fb_url") . '" />';
			echo '<meta property="og:image" content="' . of_get_option("meta_app_fb_image") . '" />';
		}
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	
	
	<link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	
	
	<!--social buttons-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/social-buttons/social-likes_flat.css">
	
	
	<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri(); ?>/_/js/jquery.cycle.all.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/social-buttons/social-likes.min.js"></script>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-4665080-43', 'auto');
	  ga('send', 'pageview');
	
	</script>
	

<meta name="viewport" content="width=1000">	
<!--<meta name="viewport" content="width=device-width initial-scale=0.5">

<script>
	    (function(doc) {
	        var viewport = document.getElementById('viewport');
	        if ( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)) {
	            doc.getElementById("viewport-new").setAttribute("content", "initial-scale=0.3,width=device-width, maximum-scale=1.0, user-scalable=yes");
	        } else if ( navigator.userAgent.match(/iPad/i) ) {
	            doc.getElementById("viewport-new").setAttribute("content", "initial-scale=0.8,width=device-width, maximum-scale=1.0, user-scalable=yes");
	        }
	        else {
	            doc.getElementById("viewport-new").setAttribute("content", "initial-scale=0.8,width=device-width, maximum-scale=1.0, user-scalable=yes");
	        }
	    }(document));
	</script>-->
	
	
<!-- Fav e touch icons -->
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

<a href="https://plus.google.com/111493846058309412571" style="display: none;" rel="publisher">Google+</a>

</head>

<body <?php body_class(); ?>>
	
	<!-- not needed? up to you: http://camendesign.com/code/developpeurs_sans_frontieres -->
	<div id="content">
	<div id="wrapper">
		
		<header id="header" role="banner">
			<div class="top">
				<div class="master-image">
					<div class="watermark">
						<div class="description"><?php bloginfo( 'description' ); ?></div>
					</div>
					<?
					
					$rows = get_field( 'immagini_di_testata' );  //this is the ACF instruction to get everything in the repeater field
						
						if($rows) {
							?>
							<div class="slideshow">
							<?
							foreach($rows as $row) {
								$image = $row['immagine']['sizes']['immagine-di-testata'];
							?>
							<img class="image-class" alt="" src="<?php echo $image; ?>" />
							<?
							}
							?>
							</div>
							<?
						}
						
						else {
							$rows = get_field( 'immagini_di_testata' , 5);
							
							if($rows) {
								?>
								<div class="slideshow">
								<?
								foreach($rows as $row) {
									$image = $row['immagine']['sizes']['immagine-di-testata'];
								?>
								<img class="image-class" alt="" src="<?php echo $image; ?>" />
								<?
								}
								?>
								</div>
								<?
							}
						}
						
					?>
					 		
					 		
				</div>
				<div class="bar left yellow"></div>
				<div class="bar right red"></div>
			</div>
			<div class="bottom">
				<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"></a>
				<div class="bar left red"></div>
				<div class="bar right yellow"></div>
			</div>
			<!--<div class="description"><?php bloginfo( 'description' ); ?></div>-->
		</header>

		<nav id="nav" role="navigation">
			<div class="bar left yellow"></div>
			<div class="bar right red"></div>
			<?php wp_nav_menu( array('menu' => 'primary') ); ?>
		</nav>
		
		
		<div id="page-content">
			<div class="bar left red"></div>
			<div class="bar right yellow"></div>
			
			<?
			
			if (!is_page_template('page-blog.php') && !is_archive() && !is_category()) {
				
				//echo('is not archive');
				
				if( get_field('titolo_pagina'))
				{	
				
				$title = get_field('titolo_pagina');
				?>
				<div class="page-head">
				<h1 class="page-title"><? echo($title); ?></h1>
				</div>
				<?php
				
				}
				
				else {
					?>
					<div class="page-head">
					<h1 class="page-title"><?php the_title(); ?></h1>
					</div>
					<?
				}
			}
			
			else {
				//echo('is archive');
			}
			
			
			
			
			?>
			
		
		
		
		
		
		
		
		

