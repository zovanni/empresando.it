<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>

		<div class="post-list">
		
		<?php if (have_posts()) : ?>
		
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<div class="archive-title">
			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h2><?php _e('Archive for the','html5reset'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php _e('Category','html5reset'); ?></h2>
			
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h2><?php _e('Posts Tagged','html5reset'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h2>
			
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h2><?php _e('Archive for','html5reset'); ?> <?php the_time('F jS, Y'); ?></h2>
			
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h2><?php _e('Archive for','html5reset'); ?> <?php the_time('F, Y'); ?></h2>
			
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h2 class="pagetitle"><?php _e('Archive for','html5reset'); ?> <?php the_time('Y'); ?></h2>
			
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h2 class="pagetitle"><?php _e('Author Archive','html5reset'); ?></h2>
			
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2 class="pagetitle"><?php _e('Blog Archives','html5reset'); ?></h2>
			<?php } ?>
			
			
			</div>
			
			<?php post_navigation(); ?>
			
			<?php while (have_posts()) : the_post(); ?>
			
				<div class="post">
					<div class="page-head">
					<h1 class="page-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
					</div>
					
					<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
						
						
						<?php posted_on(); ?>
					
						<div class="entry">
							<?php 
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								?>
								<div class="featured-image">
								<?
								the_post_thumbnail( 'thumbnail' );
								?>
								</div>
								<?
							} 
							?>
							<?php the_excerpt(); ?>
						</div>
					
						<footer class="postmetadata">
							<?php the_tags(__('Tags: ','html5reset'), ', ', '<br />'); ?>
							<!--<?php _e('Posted in','html5reset'); ?> <?php the_category(', ') ?> |--> 
						</footer>
					
					</article>
				</div>
			
			<?php endwhile; ?>
			
			<?php post_navigation(); ?>
			
			<?php else : ?>
			
			<h2><?php _e('Nothing Found','html5reset'); ?></h2>
		
		<?php endif; ?>
		</div>

<?php get_footer(); ?>
