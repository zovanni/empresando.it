<?php
/**
 * Template Name: Contattaci
 */
 
 get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<article class="post" id="post-<?php the_ID(); ?>">

			<!--<h2><?php the_title(); ?></h2>-->

			<div class="entry">
				
				<?php the_content(); ?>
				
				
				<div class="businesscard">
					<div class="txt">
					
					<? 
					if (get_field('txt_businesscard')) {
						$txt = get_field('txt_businesscard');
						echo($txt); 
					}
					
					 ?>
					</div>
				</div>
				
				<?php echo do_shortcode( '[contact-form-7 id="126" title="Contattaci"]' ) ?>
				
				
				<?php wp_link_pages(array('before' => __('Pages: ','html5reset'), 'next_or_number' => 'number')); ?>

				<?php include (TEMPLATEPATH . '/social-buttons/share.php'); ?>
			</div>

			<?php edit_post_link(__('Edit this entry','html5reset'), '<p>', '</p>'); ?>

		</article>
		
		<?php endwhile; endif; ?>

<?php get_footer(); ?>