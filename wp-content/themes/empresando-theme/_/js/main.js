// Browser detection for when you get desparate. A measure of last resort.
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }


// remap jQuery to $
(function($){

	/* trigger when page is ready */
	$(document).ready(function (){
		
		$('.slideshow').cycle({
				fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
				speed:    1000, 
				timeout:  5000
			});
			
		// Initialize the Lightbox and add rel="gallery" to all gallery images when the gallery is set up using [gallery link="file"] so that a Lightbox Gallery exists
		$(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.jpeg'], .gallery a[href$='.gif']").attr('rel','gallery').fancybox();
		
		// your functions go here
		
//		$('.social-likes').each(function () {
//			var title = $(this).attr('data-title');
//			var _url = $(this).attr('data-url');
//			
//			console.log(title)
//			$(this).hide();
//			
//			$(this).socialLikes({
//			    url: _url,
//			    title: 'pagina'
//			    ,singleTitle: 'Share it!'
//			});
//		})
		
		
	});


	/* optional triggers

	$(window).load(function() {

	});

	$(window).resize(function() {

	});

	*/
	

})(window.jQuery);