<?

if( get_field('titolo_pagina')) {
	$title = get_field('titolo_pagina');
}

else {
	$title = get_the_title();
}
 ?>

<div class="social-likes" data-url="<?php the_permalink() ?>" data-title="<?php echo($title); ?>" data-single-title="Share">

	<div class="facebook" title="Share link on Facebook">Facebook</div>
	<div class="twitter" title="Share link on Twitter">Twitter</div>
	<div class="plusone" title="Share link on Google+">Google+</div>
</div>